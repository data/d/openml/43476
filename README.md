# OpenML dataset: 3-million-Sudoku-puzzles-with-ratings

https://www.openml.org/d/43476

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Overview
This dataset contains 3 million Sudoku puzzles and their solutions. The level of difficulty varies -- some can be solved easily by a beginner, while others will challenge experienced solvers. Most puzzles have between 23 and 26 clues. The minimum number of clues in the dataset is 19, and the maximum is 31. It has been shown that 17 is the minimum number of clues for a valid, uniquely solvable Sudoku puzzle. However, these puzzles are difficult to find, so they are not included in our dataset.
Each row of the dataset includes the number of clues and an estimated difficulty rating. The difficulty rating is computed by an automated solver and it is based on the average search tree depth over 10 attempts. 43 of the puzzles have a difficulty of zero, meaning that it can be solved using a simple scanning technique. The highest difficulty rating is 8.5.
The puzzles were generated using Blagovest Dachev's Sudoku generator and solver, at https://github.com/dachev/sudoku.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43476) of an [OpenML dataset](https://www.openml.org/d/43476). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43476/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43476/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43476/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

